#!/usr/bin/env node
import 'source-map-support/register';
import {ZeroToHeroApp} from '../lib/zero-to-hero';

new ZeroToHeroApp();
