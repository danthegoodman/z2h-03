import * as cdk from '@aws-cdk/core';
import {CfnOutput, Duration} from '@aws-cdk/core';
import {
  Cluster,
  Compatibility,
  ContainerImage,
  FargateService,
  LogDriver,
  TaskDefinition
} from "@aws-cdk/aws-ecs";
import {Vpc} from "@aws-cdk/aws-ec2";
import {LogGroup} from "@aws-cdk/aws-logs";
import {Role} from "@aws-cdk/aws-iam";
import {
  ApplicationLoadBalancer,
  ListenerAction,
  ListenerCondition
} from "@aws-cdk/aws-elasticloadbalancingv2";

export class ZeroToHeroApp extends cdk.App {
  constructor() {
    super();

    //Fix this line by replacing null with your name.
    const myName: string = null;
    new ZeroToHeroStack(this, 'ZeroToHero-'+myName, {
      env: { account: '711953400095', region: 'us-east-1' },
    })
  }
}

export class ZeroToHeroStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const defaultVpc = Vpc.fromLookup(this, 'Default', {
      isDefault: true
    });
    const taskRole = Role.fromRoleArn(this, 'TaskRole', 'arn:aws:iam::711953400095:role/zero-to-hero-ecs-task')
    const executionRole = Role.fromRoleArn(this, 'ExecutionRole', 'arn:aws:iam::711953400095:role/ecsTaskExecutionRole')

    const cluster = new Cluster(this, 'Cluster', {
      vpc: defaultVpc,
    })

    const loadBalancer = new ApplicationLoadBalancer(this, 'LoadBalancer', {
      vpc: defaultVpc,
      internetFacing: true,
    });
    const listener = loadBalancer.addListener('Http', {
      port: 80,
    })
    listener.addAction('Default', {
      action: ListenerAction.fixedResponse(400, {
        messageBody: "Missing known path prefix."
      })
    })

    const logGroup = new LogGroup(this, 'LogGroup', {})
    const taskDefinition = new TaskDefinition(this, 'TaskDef', {
      compatibility: Compatibility.FARGATE,
      cpu: "256",
      memoryMiB: "512",
      taskRole,
      executionRole,
    })
    taskDefinition.addContainer('server', {
      image: ContainerImage.fromRegistry('danthegoodman/z2h-02:latest'),
      portMappings: [{containerPort: 80}],
      logging: LogDriver.awsLogs({
        logGroup,
        streamPrefix: 'my-logs',
      }),
      environment: {
        DISPLAY_NAME: "Frank",
        S3_BUCKET: "os-zero-to-hero",
        S3_PATH: "frank/image.png"
      }
    })
    const service = new FargateService(this, 'Service', {
      cluster,
      desiredCount: 1,
      taskDefinition,
      assignPublicIp: true,
    })

    listener.addTargets('HttpService', {
      priority: 1,
      conditions: [
        ListenerCondition.pathPatterns(['/frank/*'])
      ],
      deregistrationDelay: Duration.seconds(15),
      port: 80,
      targets: [
        service.loadBalancerTarget({
          containerName: 'server',
          containerPort: 80,
        })
      ]
    });

    new CfnOutput(this, 'HttpAddress', {
      value: 'http://' + loadBalancer.loadBalancerDnsName + "/frank/",
    })
  }
}
